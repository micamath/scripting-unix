# !/usr/bin/python
import os
import re
import argparse

# create a verification type for argparse
def directory(directory):
    if(os.path.isdir(directory)):
        return directory
    else: 
        raise argparse.ArgumentTypeError('{} is not a valid directory'.format(directory))

# find all the elements between quotes or double quotes in a file
def find_string(pathname):
    pattern = re.compile('\".*?\"|\'.*?\'')
    try: 
        with open(pathname,'r') as file:
            for line in file:
                elements = pattern.findall(line)
                for element in elements:
                    if args.path:
                        print(pathname, end="\t")
                        
                    print(element)
    except Exception as e:
        pass
       
                
if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('repository', type = directory)
        parser.add_argument("--all", "-a",  help = "considerate hidden files", action = 'store_true')

        parser.add_argument("--path", "-p", help = "give the found file path from the directory in argument", action = 'store_true')
        parser.add_argument("--suffix", action = "append", help = "find strings in files with given suffix") # creates a list if there are several distinct suffixes
        args = parser.parse_args()

    except argparse.ArgumentTypeError as e:
        print("Unknow option")

    for root, dirs, filenames in os.walk(args.repository):
        if root != '.' and not args.all and root.startswith('.') :
            continue
        for filename in filenames:

            if args.suffix:
                if not filename.endswith(tuple(args.suffix)):
                    continue
            if not args.all:
                if filename.startswith('.'):
                    continue

            pathname = os.path.join(root, filename)
            find_string(pathname)





