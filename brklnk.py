# !/usr/bin/python

import requests 
import urllib.request
from bs4 import BeautifulSoup
import argparse

def build_coherent_url(url_to_test, parent_url):
    try:
        if not url_to_test.startswith('http') and url_to_test[0].isalpha():
            if not parent_url.endswith('/') :
                url_to_test = '/' + url_to_test
        
            elif url_to_test.startswith('/') or url_to_test.startswith('#'):
                if parent_url.endswith('/'):
                    url_to_test = url_to_test[1::]

            url_to_test = parent_url + url_to_test

        return url_to_test


    except AttributeError as e:
        print("{}: not a valid type URL".format(url_to_test))     

def brklnk(url, depth):

    try:
        correct_urls = []
        with urllib.request.urlopen(url) as page:
            html = page.read()
            soup = BeautifulSoup(html, 'html.parser')

            for link in soup.find_all('a'):
                url_to_test = link.get('href')

                if url_to_test is not None: 
                    coherent_url_to_test = build_coherent_url(url_to_test, url)

                if coherent_url_to_test.startswith('http'):
                    r = requests.get(coherent_url_to_test)
                    if r.status_code != 200:
                        print(coherent_url_to_test, r.status_code)
                    elif depth > 1:
                        correct_urls.append(coherent_url_to_test)

            if(depth > 1):
                for new_url in correct_urls:
                    brklnk(new_url, depth - 1)              

    except urllib.error.URLError as e:
        print("URLError : Argument is not a valid URL")

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument('url')
    parser.add_argument("--depth", type = int, default = 1, help="level of depth you choose to evaluate")
    args = parser.parse_args()

    brklnk(args.url, args.depth)